use brewery;
db.barrels.drop();

db.barrels.insert([
	{ "barrel_id" : 1, "capacity" : 50, "beer_id" : 4845, "remaining" : 50 }
	, { "barrel_id" : 2, "capacity" : 60, "beer_id" : 4905, "remaining" : 60 }	
	, { "barrel_id" : 3, "capacity" : 70, "beer_id" : 3904, "remaining" : 70 }
	, { "barrel_id" : 4, "capacity" : 80, "beer_id" : 5522, "remaining" : 80 }
	, { "barrel_id" : 5, "capacity" : 90, "beer_id" : 5699, "remaining" : 90 }
	, { "barrel_id" : 6, "capacity" : 100, "beer_id" : 5841, "remaining" : 100 }
]);