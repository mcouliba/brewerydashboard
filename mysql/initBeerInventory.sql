use brewery;

CREATE TABLE BeerInventory (
  beer_id int NOT NULL,
  remaining_barrel int NOT NULL,
  PRIMARY KEY (beer_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO BeerInventory VALUES (4845,10), (4905,10), (3904,10), (5522,10), (5699,10), (5841,10);
