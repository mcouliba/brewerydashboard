(function () {
  'use strict';

  angular
    .module('app')
    .directive('barrelChart', BarrelChartDirective)
    .directive('barrelChartItem', BarrelChartItemDirective);

function BarrelChartDirective() {

      return {
        restrict:     "AE",
        templateUrl:  "template/barrelChart.html",
        controllerAs: 'vm' ,
        controller:   'BarrelChartController'
      }
    }; // END function BarrelChartDirective

function BarrelChartItemDirective() {
    return {
      require:  '^barrelChart',
      link:     function($scope, elem, attr, barrelChart) {
                  barrelChart.setLayout(elem, $scope.barrelInfo);
                }
    }
  }; // END function BarrelChartItemDirective

})();
