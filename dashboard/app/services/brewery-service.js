(function () {
    'use strict';

    angular
        .module('app')
        .factory('BreweryService', BreweryService);

    BreweryService.$inject = ['$http', '$q'];
    function BreweryService($http, $q) {
        var configDataVirt = {
            host : 'datavirt-brewery-demo.rhel-cdk.10.1.2.2.xip.io'
            , port : '80'
            , user : 'teiidUser'
            , password : 'redhat1!'
        };

        var vdbURL = "http://" + configDataVirt.host + ":" + configDataVirt.port + "/odata4/Brewery";
        $http.defaults.headers.common.Authorization = 'Basic ' + Base64.encode(configDataVirt.user + ":" + configDataVirt.password);
        $http.defaults.headers.common.Accept = 'application/json';

        var service = {};

        service.getListBarrelInfo = getListBarrelInfo;
        service.getBarrelConsumption  = getBarrelConsumption;
        service.getOrders  = getOrders;
        service.getTopDrinker  = getTopDrinker;
        service.fillBarrel  = fillBarrel;

        return service;
        

        function getListBarrelInfo() {
          return $http({
              url : vdbURL + '/Brewery/barrelInfo?$format=json'
              ,method : 'GET'
            }).then(handleSuccess, handleError('Error BreweryService.getListBarrelInfo'));
        }

        function getForUpdateBarrelInfo(barrel_id) {
          return $http({
              url : vdbURL + '/Brewery/barrelInfo?$format=json&$filter=barrel_id eq ' + barrel_id
              ,method : 'GET'
            }).then(handleSuccess, handleError('Error BreweryService.getBarrelInfo'));
        }

        function getBarrelConsumption(barrel_id) {
          return $http({
              url : vdbURL + '/Brewery/barrelConsumption?$format=json&$filter=barrel_id eq ' + barrel_id
              ,method : 'GET'
            }).then(handleSuccess, handleError('Error BreweryService.getBarrelConsumption'));
        }

        function getOrders() {
          return $http({
              url : vdbURL + '/Brewery/orders?$format=json'
              ,method : 'GET'
            }).then(handleSuccess, handleError('Error BreweryService.getOrders'));
        }

        function getTopDrinker(barrel_id, topNumber) {
          return $http({
              url : vdbURL
                      + '/Brewery/topDrinker?$format=json&$orderby=litres_drinked desc&$top='
                      + topNumber
                      + '&$filter=barrel_id eq '
                      + barrel_id
              ,method : 'GET'
            }).then(handleSuccess, handleError('Error BreweryService.getTopDrinker'));
        }

        function fillBarrel(barrel_id, capacity) {
          $http({
              url : vdbURL + '/Brewery/barrelConsumption(' + barrel_id + ')'
              , method : 'PATCH'
              , headers: {
                  'Content-Type': 'application/json; odata.metadata=minimal'
                }
              , data : {
                  "remaining" : capacity
                }
            });
        }

        // -----------------------------
        // private functions
        // -----------------------------
        function handleSuccess(result) {
            return result.data;
        }

        function handleError(error) {
            return function () {
                alert(error);
                return { success: false, message: error };
            };
        }

        function getConfigDataVirt() {
            $http.get('/configDataVirt')
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }   
    }

    // Base64 encoding service used by BreweryService
    var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };

})();
