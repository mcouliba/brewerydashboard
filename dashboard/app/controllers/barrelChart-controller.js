(function () {
  'use strict';

  angular
    .module('app')
    .controller('BarrelChartController', BarrelChartController);

  BarrelChartController.$inject = ['$interval', 'BreweryService'];

  function BarrelChartController($interval, BreweryService) {
    var vm = this;

    vm.fillBarrel = BreweryService.fillBarrel;
    vm.setLayout = setLayout;

    BreweryService.getListBarrelInfo()
        .then(function (result) {
            if (result !== null && result.value !== null && result.value.length > 0) {
                vm.listBarrelInfo = result.value;
            }
        });

    function setLayout(elem, barrelInfo) {
      var charts = configDonutChart(elem);
      dataChart(elem, barrelInfo, charts);
      $interval(function() {
          dataChart(elem, barrelInfo, charts);
        }, 2000); // End $interval
    }; // END function setLayout

    function configDonutChart(elem) {
        var d3el = d3.select(elem[0]);
        // --------- Donut Chart ---------
        var donutConfig = $().c3ChartDefaults().getDefaultDonutConfig();
        donutConfig.bindto = d3el.select(".chart-pf-donut");
        donutConfig.color =  {
            pattern : ["#cc0000", "#D1D1D1"]
        };
        donutConfig.data = {
          type: "donut",
          columns: [
            ["Used", 0, ]
            ["Remaining", 0]
          ],
          groups: [
            ["used", "remaining"]
          ],
          order: null
        };

        donutConfig.tooltip = {
          contents: function (d) {
            return '<span class="donut-tooltip-pf" style="white-space: nowrap;">' +
                    Math.round(d[0].ratio * 100) + '% of litres' + d[0].name +
                    '</span>';
          }
        };

        var donutChart = c3.generate(donutConfig);

        // --------- End Donut Chart ---------

        // --------- Sparkline Chart ---------
        var sparklineConfig = $().c3ChartDefaults().getDefaultSparklineConfig();
        sparklineConfig.bindto = d3el.select(".chart-pf-sparkline");
        sparklineConfig.data = {
          columns: [],
          type: 'area'
        };
        var sparklineChart = c3.generate(sparklineConfig);
        //var sparklineData = ['%'];
        // --------- End Sparkline Chart ---------


        // --------- Gauge Chart ---------
        var gaugeConfig = {
          bindto : d3el.select(".gauge-chart"),
            data: {
                columns: [],
                type: 'gauge'
            },
            gauge: {
               label: {
                   format: function(value, ratio) {
                       return value.toFixed(1) + '˚C';
                   },
                   show: false // to turn off the min/max labels.
               },
        //    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
              max: 10, // 100 is default
              units: '˚C',
        //    width: 39 // for adjusting arc thickness
            },
            color: {
                pattern: ['#a18fff', '#7cdbf3', '#f39d3c', '#cc0000'], // the three color levels for the percentage values.
                threshold: {
                    units: '˚C',
                    max: 10,
                    values: [30, 50, 70, 100]
                }
            },
            size: {
                height: 180
            }
        };
        var gaugeChart = c3.generate(gaugeConfig);

        // --------- End Gauge Chart ---------

        // --------- Top Drinker Chart ---------
        var c3ChartDefaults = $().c3ChartDefaults();

        var horizontalBarChartConfig = $().c3ChartDefaults().getDefaultBarConfig();
        horizontalBarChartConfig.bindto = d3el.select(".hbar-chart");
        horizontalBarChartConfig.axis = {
          rotated: true,
          x: {
            categories: [],
            type: 'category'
          }

          ,y: {
            padding: {
              top: 100,
              bottom: 100
            }
          }
        };

        horizontalBarChartConfig.data = {
          type: 'bar',
          columns: [],
          color: function (color, d) {
            var colors = ['#f5c12e', '#C0C0C0', '#CD7F32', '#cfe7cd', '#c7bfff']
            return colors[d.index];
          }
        };
        var horizontalBarChart = c3.generate(horizontalBarChartConfig);
        // --------- End Top Drinker Chart ---------


        return {
          "donutChart" : donutChart
          , "sparklineChart" : sparklineChart
          , "gaugeChart" : gaugeChart
          , "horizontalBarChart" : horizontalBarChart
        }
      }; // END function configChart

      function dataChart(elem, barrelInfo, charts) {
        var d3el = d3.select(elem[0]);

        BreweryService.getBarrelConsumption(barrelInfo.barrel_id)
            .then(function (result) {
                if (result !== null && result.value !== null && result.value.length > 0) {
                    var barrelConsumption = result.value[0];
                    var capacity = barrelInfo.capacity;
                    var remaining = barrelConsumption.remaining; //(Math.random() * barrelInfo.capacity).toFixed(2);//
                    var ratio = remaining / capacity;
                    var temperature = 0;
                    if (barrelConsumption.temperature !== null) {
                      temperature = barrelConsumption.temperature.toFixed(2);
                    }

                    var donutChartTitle = d3el.select(".chart-pf-donut").select('text.c3-chart-arcs-title');
                    donutChartTitle.text("");
                    donutChartTitle.insert('tspan').text(remaining.toFixed(2)).classed('donut-title-big-pf', true).attr('dy', 0).attr('x', 0);
                    donutChartTitle.insert('tspan').text(" of " + capacity + " Litres").classed('donut-title-small-pf', true).attr('dy', 20).attr('x', 0);

                    charts['donutChart'].load({
                      columns: [
                        ["Used", capacity - remaining],
                        ["Remaining", remaining]
                      ]
                      ,colors : {
                        "Used" : "#D1D1D1"
                        , "Remaining" : function () {
                            return ratio <= 0.15 ? "#cc0000" : (ratio <= 0.50 ? "#ec7a08" : "#3f9c35");
                          }
                      }
                    });

                    var sparklineData = charts['sparklineChart'].data.values('%');

                    if (!sparklineData ) {
                      sparklineData = [];
                    }
                    sparklineData.unshift('%');
                    sparklineData.push(ratio);

                    charts['sparklineChart'].load({
                      columns: [
                        sparklineData
                      ]
                    });

                    charts['gaugeChart'].load({
                      columns: [
                        ['˚C', temperature]
                      ]
                    });
                }
            });

            BreweryService.getTopDrinker(barrelInfo.barrel_id, 5)
                .then(function (result) {
                    if (result !== null && result.value !== null && result.value.length > 0) {
                      var topList = result.value;

                      var categories = [];
                      var drinked = ['data1'];

                      for (var i = 0; i < topList.length; i++) {
                        categories.push(topList[i].firstName + ' ' + topList[i].lastName);
                        drinked.push(topList[i].litres_drinked);
                      }

                      var columnsData = [
                        drinked
                      ];


                      charts['horizontalBarChart'].load({
                        columns: [
                          drinked
                        ]
                        ,
                          categories: categories
                      });
                    }
                });
      }; // END function dataChart

  }; // END function BarrelChartController
})();
