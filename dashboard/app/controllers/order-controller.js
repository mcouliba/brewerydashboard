(function () {
  'use strict';

  angular
    .module('app')
    .controller('OrderController', OrderController);

  OrderController.$inject = ['$rootScope', '$interval', 'BreweryService'];

  function OrderController($rootScope, $interval, BreweryService) {
    var vm = this;

    function updateOrders() {
      BreweryService.getOrders()
          .then(function (result) {
              if (result !== null && result.value !== null && result.value.length > 0) {
                  vm.listOrders = result.value;
              }
          });
    }
    // ---------------------------------
    // MAIN
    // ---------------------------------
    updateOrders();

    $interval(function() {
       updateOrders();
    }, 5000);

  }; // END function Controller
})();
