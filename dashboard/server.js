var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(__dirname + '/app'));

// Add headers
app.use(function (req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

app.get('/', function(req, res){
  res.redirect('/index.html');
  //res.sendFile(path.join(__dirname + '/index.html'));
});

// Start the server
var port = process.env.PORT || 8080,
    ip   = process.env.IP  || '0.0.0.0';
app.listen(port, ip);

console.log("[CUSTOM LOGS] Start internal server '%s:%s'", ip, port);
