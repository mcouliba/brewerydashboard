package brewery.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import brewery.helper.BreweryDatabase;
import brewery.manager.BarrelCacheManager;
import brewery.manager.OrderCacheManager;
import brewery.model.Barrel;
import brewery.model.Beer;
import brewery.model.BeerQuantity;
import brewery.model.Customer;
import brewery.model.OrderTemp;

public class BreweryLife {
	private static final Random RANDOM = new Random();
	private static final int GENERATE_INTERVAL = 10; // for how long the data should be generated (min)
	private static final int GENERATE_PERIOD = 1000; // how often data should be generated, in ms
	   
	public static void main(String[] args) throws InterruptedException, Exception {
		OrderCacheManager orderCacheManager = new OrderCacheManager();
		BarrelCacheManager barrelCacheManager = new BarrelCacheManager();
		
		// Clear caches
		orderCacheManager.clear();
		barrelCacheManager.clear();
		
		// Open connection
		BreweryDatabase.openConnection();
		
		// Insert some data into the cache
	    TimerTask randOrder = new OrderGenerator(orderCacheManager);
	    TimerTask randBarrel = new BarrelGenerator(barrelCacheManager);
	    Timer timer = new Timer(true);
	    timer.schedule(randOrder, 0, GENERATE_PERIOD); // start generating of random orders
	    timer.schedule(randBarrel, 0, GENERATE_PERIOD); // start generating of random temperatures
	    System.out.println("Inserting Orders and Barrels");
	    
	    // Generate data for specified interval
	    Thread.sleep(GENERATE_INTERVAL * 60 * 1000);
		randOrder.cancel();
		randBarrel.cancel();
		orderCacheManager.stop();
		barrelCacheManager.stop();
		BreweryDatabase.closeConnection();
		System.out.println("DONE! No more data for you.");
		System.exit(0);
		
	}
	
	private static class OrderGenerator extends TimerTask {

	      private int orderNo = 0;
	      private List<Beer> beers;
	      private List<Customer> customers;
	      
	      private final OrderCacheManager orderCacheManager;

	      public OrderGenerator(OrderCacheManager orderCacheManager) {
	    	  this.orderCacheManager = orderCacheManager;
	    	  int count = 0;
	    	  beers = new ArrayList<Beer>();
	    	  customers = new ArrayList<Customer>();
	    	  
	    	  while (count < 15) {
	    		  customers.add(new Customer("client", "F" + count++));
	    	  }
	    	  
	    	  beers.add(new Beer(4845));
	    	  beers.add(new Beer(4905));
	    	  beers.add(new Beer(3904));
	    	  beers.add(new Beer(5522));
	    	  beers.add(new Beer(5699));
	    	  beers.add(new Beer(5841));
	      }

	      @Override
	      public void run() {
	         Beer beer = beers.get(RANDOM.nextInt(beers.size()));
	         Customer customer = customers.get(RANDOM.nextInt(customers.size()));
	         int randomQuantity = RANDOM.nextInt(2);
	         BeerQuantity quantity = BeerQuantity.values()[randomQuantity];
	         
	         
//	         Order newOrder = new Order(orderNo++
//		 				, customer
//		 				, beer
//		 				, quantity);
	         
	         OrderTemp newOrderTemp = new OrderTemp(orderNo++
	        		 				, customer.getFirstName()
	        		 				, customer.getLastName()
	        		 				, beer.getId()
	        		 				, randomQuantity);
	         
	         
//	         orderCacheManager.orderBeer(newOrder);
	         orderCacheManager.orderTempBeer(newOrderTemp);
	         BreweryDatabase.serveDrink(beer.getId(), quantity);
	      }
	   }
	
	private static class BarrelGenerator extends TimerTask {

	      private static final int TEMP_MAX = 10; // maximum temperature
	      private static final Integer[] barrels = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	      private final BarrelCacheManager barrelCacheManager;
	      
	      public BarrelGenerator(BarrelCacheManager barrelCacheManager) {
	    	  this.barrelCacheManager = barrelCacheManager;
	      }

	      @Override
	      public void run() {
	         int barrel_id = barrels[RANDOM.nextInt(barrels.length)];
	         float temp = RANDOM.nextFloat() * TEMP_MAX;
	         Barrel newBarrel = new Barrel(barrel_id, temp);
	         
	         barrelCacheManager.updateBarrel(newBarrel);
	      }
	   }

}
