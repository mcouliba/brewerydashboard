package brewery.model;

import java.io.Serializable;

import org.infinispan.protostream.annotations.ProtoField;

/**
 * Represents an order in the brewery. order Objects are stored in the cache.
 * 
 * @author mcouliba
 *
 */
public class OrderTemp implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String firstName;
	private String lastName;
	private int beerId;
	private int beerQuantity;
	
	public OrderTemp(final int id
			, final String firstName, String lastName
			, final int beerId
			, final int beerQuantity) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.beerId = beerId;
		this.beerQuantity = beerQuantity;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getBeerId() {
		return beerId;
	}

	public void setBeerId(int beerId) {
		this.beerId = beerId;
	}

	public int getBeerQuantity() {
		return beerQuantity;
	}

	public void setBeerQuantity(int beerQuantity) {
		this.beerQuantity = beerQuantity;
	}
}
