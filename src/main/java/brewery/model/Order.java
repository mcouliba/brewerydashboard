package brewery.model;

import java.io.Serializable;

import org.infinispan.protostream.annotations.ProtoField;

/**
 * Represents an order in the brewery. order Objects are stored in the cache.
 * 
 * @author mcouliba
 *
 */
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ProtoField(number = 1, required = true)
	private int id;
	@ProtoField(number = 2, required = true)
	private Customer customer;
	@ProtoField(number = 3, required = true)
	private Beer beer;
	@ProtoField(number = 4, required = true)
	private BeerQuantity beerQuantity;
	
	public Order(final int id, final Customer customer, final Beer beer, final BeerQuantity quantity) {
		this.id = id;
		this.customer = customer;
		this.beer = beer;
		this.beerQuantity = quantity;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Beer getBeer() {
		return beer;
	}

	public void setBeer(Beer beer) {
		this.beer = beer;
	}

	public BeerQuantity getQuantity() {
		return beerQuantity;
	}

	public void setQuantity(BeerQuantity beerQuantity) {
		this.beerQuantity = beerQuantity;
	}
}
