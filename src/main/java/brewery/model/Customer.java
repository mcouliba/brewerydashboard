package brewery.model;

import java.io.Serializable;

/**
 * Represents a customer in the brewery. Customer Objects are stored in the cache.
 * 
 * @author mcouliba
 *
 */
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	
	public Customer(final String firstName, final String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
