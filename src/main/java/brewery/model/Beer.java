package brewery.model;

import java.io.Serializable;

/**
 * Represents a beer in the brewery. Beer Objects are stored in the cache.
 * 
 * @author mcouliba
 *
 */
public class Beer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	
	
	public Beer(final int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
