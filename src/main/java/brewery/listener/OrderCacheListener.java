package brewery.listener;

import org.infinispan.client.hotrod.annotation.ClientCacheEntryCreated;
import org.infinispan.client.hotrod.annotation.ClientListener;
import org.infinispan.client.hotrod.event.ClientCacheEntryCreatedEvent;

@ClientListener
public class OrderCacheListener {
	
	 @ClientCacheEntryCreated
     public void entryCreated(ClientCacheEntryCreatedEvent<Integer> event) {
		 System.out.println("Receiving new order " + event.getKey());
     }

//     @ClientCacheEntryModified
//     public void entryModified(ClientCacheEntryModifiedEvent<String> event) {
//        if (watchedPlaces.contains(event.getKey()))
//           updateAction(event.getKey());
//     }
}
