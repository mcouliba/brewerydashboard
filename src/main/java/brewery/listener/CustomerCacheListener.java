package brewery.listener;

import org.infinispan.client.hotrod.annotation.ClientCacheEntryCreated;
import org.infinispan.client.hotrod.annotation.ClientCacheEntryModified;
import org.infinispan.client.hotrod.annotation.ClientListener;
import org.infinispan.client.hotrod.event.ClientCacheEntryCreatedEvent;
import org.infinispan.client.hotrod.event.ClientCacheEntryModifiedEvent;

@ClientListener
public class CustomerCacheListener {
	
	 @ClientCacheEntryCreated
     public void entryCreated(ClientCacheEntryCreatedEvent<Integer> event) {
		 System.out.println("Add new customer '" + event.getKey() + "'");
     }

     @ClientCacheEntryModified
     public void entryModified(ClientCacheEntryModifiedEvent<Integer> event) {
    	 System.out.println("Update customer '" + event.getKey() + "'");
     }
}
