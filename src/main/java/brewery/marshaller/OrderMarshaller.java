package brewery.marshaller;

import java.io.IOException;

import org.infinispan.protostream.MessageMarshaller;

import brewery.model.Beer;
import brewery.model.BeerQuantity;
import brewery.model.Customer;
import brewery.model.Order;

public class OrderMarshaller implements MessageMarshaller<Order> {

   @Override
   public String getTypeName() {
      return "brewery.Order";
   }

   @Override
   public Class<Order> getJavaClass() {
      return Order.class;
   }

   @Override
   public Order readFrom(ProtoStreamReader reader) throws IOException {
	   
		int id = reader.readInt("id");
		Customer customer = reader.readObject("customer", Customer.class);
		Beer beer = reader.readObject("beer", Beer.class);
		BeerQuantity beerQuantity = reader.readObject("beerQuantity", BeerQuantity.class);
 
		Order order = new Order(id, customer, beer, beerQuantity);

		return order;
   }

   @Override
   public void writeTo(ProtoStreamWriter writer, Order order) throws IOException {
      writer.writeInt("id", order.getId());
      writer.writeObject("customer", order.getCustomer(), Customer.class);
      writer.writeObject("beer", order.getBeer(), Beer.class);
      writer.writeObject("beerQuantity", order.getQuantity(), BeerQuantity.class);
   }
}

