/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package brewery.marshaller;

import org.infinispan.protostream.EnumMarshaller;

import brewery.model.BeerQuantity;

public class BeerQuantityMarshaller implements EnumMarshaller<BeerQuantity> {

   @Override
   public Class<BeerQuantity> getJavaClass() {
      return BeerQuantity.class;
   }

   @Override
   public String getTypeName() {
      return "brewery.Order.BeerQuantity";
   }

   @Override
   public BeerQuantity decode(int enumValue) {
      switch (enumValue) {
         case 0:
            return BeerQuantity.HALF_A_PINT;
         case 1:
            return BeerQuantity.PINT;
      }
      return null;
   }

   @Override
   public int encode(BeerQuantity beerQuantity) {
      switch (beerQuantity) {
         case HALF_A_PINT:
            return 0;
         case PINT:
            return 1;
      }

      throw new IllegalArgumentException("Unexpected PhoneType value : " + beerQuantity);
   }
}
