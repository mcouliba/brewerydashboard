package brewery.helper;

import java.io.IOException;
import java.util.Properties;

public class PropertyManager {
	
	private final static String PROPERTIES_FILE = "datagrid.properties";
	
	private Properties properties = new Properties();
	
	public PropertyManager(){
		try {
			properties.load(this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getHosts() {
		return properties.getProperty("datagrid.hosts", "localhost:11322");
	}
}
