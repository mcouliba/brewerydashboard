package brewery.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import brewery.model.BeerQuantity;

public class BreweryDatabase {
	 	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "org.teiid.jdbc.TeiidDriver";
	   static final String DB_URL = "jdbc:teiid:Brewery@mm://localhost:31000";

	   //  Database credentials
	   static final String USER = "teiidUser";
	   static final String PASS = "redhat1!";

	   private static Connection connection;

	   private BreweryDatabase() {

	   }

	   public static void openConnection() throws ClassNotFoundException, SQLException {
		   if (connection == null) {
		   		//Register JDBC driver
		      Class.forName(JDBC_DRIVER);

		      // Open a connection
		      connection = DriverManager.getConnection(DB_URL,USER,PASS);
		   }
	   }

	   public static void closeConnection() throws SQLException {
		   if (connection != null) {
		   		connection.close();
		   }
	   }

	   public static void serveDrink(final int beerId, final BeerQuantity beerQuantity) {
		   Statement stmt = null;
		   double beerLitres = (beerQuantity == BeerQuantity.PINT) ? 0.568 : 0.294;
		   double newRemaining = -1;

		   try{
			   stmt = connection.createStatement();

			   String selectSql = "SELECT remaining from DS_MONGO_BREWERY.barrels where beer_id = " + beerId + ";";
			   ResultSet selectResults = stmt.executeQuery(selectSql);
		       if (selectResults.next()) {
		    	   newRemaining = selectResults.getInt(1);
		       }

		       if (newRemaining < 0) {
		    	   // ERROR
		    	   throw new Exception("Error in serveDrink : No Data found for the query " + selectSql);
		       }

		       newRemaining -= beerLitres;

			   String updateSql = "Update DS_MONGO_BREWERY.barrels SET remaining = " + newRemaining
					   + " where beer_id = " + beerId + ";";
			   stmt.executeUpdate(updateSql);

	      // Clean-up environment
		  selectResults.close();
	      stmt.close();
	   }catch(SQLException se){
	      //Handle errors for JDBC
	      se.printStackTrace();
	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(stmt!=null)
	            stmt.close();
	      }catch(SQLException se2){
	      }// nothing we can do
	   }//end try
	}
}
