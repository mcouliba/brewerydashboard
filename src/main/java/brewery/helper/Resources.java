package brewery.helper;

import javax.annotation.PreDestroy;

import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.configuration.ConfigurationBuilder;
import org.infinispan.client.hotrod.marshall.ProtoStreamMarshaller;

public class Resources {

	// Cache Managers
	private static RemoteCacheManager remoteCacheManager;
	
	@PreDestroy
	public void shutdown() {
		if (remoteCacheManager != null) {
			remoteCacheManager.stop();
			remoteCacheManager = null;
		}
	}
	
	public static RemoteCacheManager getRemoteCacheContainer(){
		if (remoteCacheManager == null) {
			PropertyManager propertyManager = new PropertyManager();
			ConfigurationBuilder configBuilder = new ConfigurationBuilder();
			configBuilder
				.addServers(propertyManager.getHosts())
				.marshaller(new ProtoStreamMarshaller());
			remoteCacheManager = new RemoteCacheManager(configBuilder.build(), true);
		}
		return remoteCacheManager;
	}
}
