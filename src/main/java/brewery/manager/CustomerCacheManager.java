package brewery.manager;

import java.io.IOException;

import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.marshall.ProtoStreamMarshaller;
import org.infinispan.protostream.FileDescriptorSource;
import org.infinispan.protostream.SerializationContext;

import brewery.helper.Resources;
import brewery.listener.CustomerCacheListener;
import brewery.marshaller.BeerMarshaller;
import brewery.marshaller.BeerQuantityMarshaller;
import brewery.marshaller.CustomerMarshaller;
import brewery.marshaller.OrderMarshaller;
import brewery.model.Customer;

public class CustomerCacheManager {
	
	private static final String CUSTOMER_CACHE_NAME = "customercache";
	private static final String PROTOBUF_DEFINITION_RESOURCE = "/proto/order.proto";
	
	RemoteCache<Integer, Customer> remoteCache;
	
	public CustomerCacheManager() throws Exception {

		RemoteCacheManager remoteCacheManager = Resources.getRemoteCacheContainer();
		remoteCache = remoteCacheManager.getCache(CUSTOMER_CACHE_NAME);
		
		//registerMarshallers(remoteCacheManager);
		registerListeners();
	}
	
	private void registerMarshallers(RemoteCacheManager remoteCacheManager) throws IOException {
		SerializationContext ctx = ProtoStreamMarshaller.getSerializationContext(remoteCacheManager);
	    ctx.registerProtoFiles(FileDescriptorSource.fromResources(PROTOBUF_DEFINITION_RESOURCE));
	    ctx.registerMarshaller(new OrderMarshaller());
	    ctx.registerMarshaller(new CustomerMarshaller());
	    ctx.registerMarshaller(new BeerMarshaller());
	    ctx.registerMarshaller(new BeerQuantityMarshaller());
	}
	
	private void registerListeners(){
		remoteCache.addClientListener(new CustomerCacheListener());
	}
    
	public Customer getCustomer(int cust_id) {
		Customer customer = (Customer) remoteCache.get(cust_id); 
		return customer;
	}
	
	public void addCustomer(Customer customer) {
		remoteCache.put(1, customer);
	}
	
	public void clear() {
		remoteCache.clear();
	}
	
	public void stop() {
		remoteCache.stop();
	}
}
